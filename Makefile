DOCKER_NAME ?= dinghao188/rcore-tutorial
.PHONY: docker build_docker

docker:
	docker run --rm -it --mount type=bind,source=$(shell pwd),destination=/mnt ${DOCKER_NAME}

build_docker: 
	docker build -t ${DOCKER_NAME} .

SUBDIR=./os
MAKE=make
all:
	cd $(SUBDIR) && $(MAKE) build
	cp ./os/target/riscv64gc-unknown-none-elf/release/os.bin ./k210.bin